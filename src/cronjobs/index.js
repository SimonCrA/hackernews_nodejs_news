import { CronJob } from 'cron'

import { saveManyNewsService } from '../services/news.service.js'

export default () => {
  const EVERY_HOUR_HACKERNEWS_FETCH = new CronJob(
    '0 */1 * * *',
    async () => {
      console.log('Retreiving data from hackernews API...')
      await saveManyNewsService().catch((_error) => {
        console.log('Something went really wrong and we could not retreive/save the news ', _error)
      })
      console.log('Data retreived and Saved in our database. :)')
    },
    null,
    true,
    'America/Caracas'
  )
  EVERY_HOUR_HACKERNEWS_FETCH.start()
}
