'use strict'

//Require the dev-dependencies
import chai from 'chai'
import chaiHttp from 'chai-http'
let should = chai.should()

chai.use(chaiHttp)
let server = 'http://localhost:3000/api/v1'

describe('/GET list news', () => {
  it('Should list 5 news from database', (done) => {
    chai
      .request(server)
      .get('/list/nodejs/news')
      .end((_err, _res) => {
        _res.should.have.status(202)
        _res.body.should.be.a('object')
        _res.body.should.have.property('ok').eql(true)
        _res.body.should.have.property('data')
        _res.body.data.should.have.lengthOf(5)
        done()
      })
  })
})
