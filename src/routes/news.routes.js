'use-strict'

import { retrieveAndSaveDataController, listNodejsNewsController, deleteNodejsNewByIdController } from '../controllers/news.controller.js'
import { listNodejsNewsMiddleware, deleteNodejsNewByIdMiddleware } from '../middlewares/news.middleware.js'

export function routesConfig(app) {
  app.get('/api/v1/load/data', retrieveAndSaveDataController)
  app.get('/api/v1/list/nodejs/news', listNodejsNewsMiddleware, listNodejsNewsController)
  app.delete('/api/v1/list/nodejs/new', deleteNodejsNewByIdMiddleware, deleteNodejsNewByIdController)
}
