import { validationResult } from 'express-validator'

import { saveManyNewsService, listNodejsNewsService, deleteNodejsNewByIdService } from '../services/news.service.js'

/**
 * Controller to retrieve data from the hackernews API.
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
export async function retrieveAndSaveDataController(req, res) {
  try {
    const DB_RES = await saveManyNewsService().catch((_error) => {
      throw _error
    })

    if (DB_RES) {
      res.status(200).json({
        ok: true,
        data: null,
        message: 'Database has been populated with news.'
      })
    }
  } catch (_error) {
    console.log(`retrieveAndSaveDataControllerError -> ${_error}`)
    res.status(500).json({
      ok: false,
      message: 'Fatal error.'
    })
  }
}

/**
 * Controller to list news stored in the database
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
export async function listNodejsNewsController(req, res) {
  try {
    let { page, limit, search } = req.query

    //Validations
    const validationErrors = validationResult(req)
    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        ok: false,
        err: validationErrors.array()
      })
    }

    const DB_NEWS = await listNodejsNewsService(page, limit, search).catch((_error) => {
      throw _error
    })

    res.status(202).json({
      ok: true,
      count: DB_NEWS.count,
      data: DB_NEWS.data,
      message: 'List of Node.js news on hackernews.'
    })
  } catch (_error) {
    console.log(`listNodejsNewsControllerError -> ${_error}`)
    res.status(500).json({
      ok: false,
      message: 'Ha ocurrido un error grave'
    })
  }
}

/**
 * Controller to delete a new from the database, passing the news's objectID
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
export async function deleteNodejsNewByIdController(req, res) {
  try {
    let { objectID } = req.query

    //Validations
    const validationErrors = validationResult(req)
    if (!validationErrors.isEmpty()) {
      return res.status(400).json({
        ok: false,
        err: validationErrors.array()
      })
    }

    await deleteNodejsNewByIdService(objectID).catch((_error) => {
      throw _error
    })

    res.status(202).json({
      ok: true,
      data: null,
      message: 'New deleted succesfully.'
    })
  } catch (_error) {
    console.log(_error)
    res.status(500).json({
      ok: false,
      message: 'Ha ocurrido un error grave'
    })
  }
}
