'use-strict'

import { body, param, query } from 'express-validator'

import { checkIfNewIsSavedAlreadyService } from '../services/news.service.js'

export const listNodejsNewsMiddleware = [
  query('page')
    .optional()
    .notEmpty()
    .withMessage('page must not be empty.')
    .isNumeric()
    .withMessage('page must be a number')
    .trim()
    .isLength({ min: 1, max: 2 })
    .withMessage('page must not be greater than 99'),
  query('limit')
    .optional()
    .notEmpty()
    .withMessage('limit must not be empty.')
    .isNumeric()
    .withMessage('limit must be a number')
    .trim()
    .isLength({ min: 1, max: 2 })
    .withMessage('limit must not be greater than 99')
]

export const deleteNodejsNewByIdMiddleware = [
  query('objectID')
    .notEmpty()
    .withMessage('objectID must not be empty.')
    .isNumeric()
    .withMessage('objectID must be a number')
    .trim()
    .custom(async (_value, { req }) => {
      try {
        const DB_NEW_FOUND = await checkIfNewIsSavedAlreadyService(_value).catch((_error) => {
          throw _error
        })
        if (!DB_NEW_FOUND) {
          throw new Error('The new you are trying to delete does not exist or it is already deleted.')
        }
        return true
      } catch (_error) {
        throw _error
      }
    })
]
