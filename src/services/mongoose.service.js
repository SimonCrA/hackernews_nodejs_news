import mongoose from 'mongoose'

/**
 * Function to create the database connection with its options
 * @returns Connection Object
 */
export async function connectDb() {
  return await mongoose
    .connect(process.env.DB_URI, {
      // autoIndex: false, // Don't build indexes
      // If not connected, return errors immediately rather than waiting for reconnect
      useCreateIndex: true,
      bufferMaxEntries: 0,
      // all other approaches are now deprecated by MongoDB:
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true,

      //auth
      dbName: process.env.DB_NAME,
      user: process.env.DB_USERNAME,
      pass: process.env.DB_PASSWORD
    })
    .catch((_error) => {
      throw _error
    })
}
