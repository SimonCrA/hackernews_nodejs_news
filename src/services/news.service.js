'use-strict'
import axios from 'axios'
import News from '../models/news.model.js'

const HACKERNEWS_URL = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs#'

/**
 * Service to retrieve data from the external URL provided
 * @returns Object
 */
let retrieveDataHackerNewApiService = async () => {
  try {
    //Check if the email already exists in the database
    const { data } = await axios.get(HACKERNEWS_URL).catch((_error) => {
      throw _error
    })
    return data
  } catch (_error) {
    throw new Error(`newsServiceError01 -> ${_error}`)
  }
}

/**
 * Service to store the data retrieved into the database
 * @returns Boolean
 */
let saveManyNewsService = async () => {
  try {
    const NEWS_DATA = await retrieveDataHackerNewApiService().catch((_error) => {
      throw _error
    })

    await News.insertMany(NEWS_DATA.hits).catch((_error) => {
      throw _error
    })
    return true
  } catch (_error) {
    throw _error
  }
}

/**
 * Service to retrieve list of news stored in the database, with pagination
 * @param {Number} _page Number of the document the query will start to retrieve data
 * @param {Number} _limit Number of documents by request
 * @param {String} _search Filters to be applied to the query, It can filter by "tags", "author", and "title"
 * @returns Object
 */
let listNodejsNewsService = async (_page = 0, _limit = 5, _search) => {
  try {
    // It removes _id of all documents and subdocuments in the response
    const REMOVE_IDS = {
      _id: 0,
      '_highlightResult._id': 0,
      '_highlightResult.author._id': 0,
      '_highlightResult.comment_text._id': 0,
      '_highlightResult.story_title._id': 0,
      __v: 0
    }

    // filters
    const AUTHOR_FILTER = _search ? { author: _search } : {}
    const TITLE_FILTER = _search ? { story_title: { $regex: _search, $options: 'i' } } : {}
    const TAGS_FILTER = _search ? { _tags: _search } : {}
    const FINAL_QUERY = [AUTHOR_FILTER, TITLE_FILTER, TAGS_FILTER]

    const NEWS_LIST = await News.find({ $or: FINAL_QUERY }, REMOVE_IDS)
      .limit(parseInt(_limit))
      .skip(parseInt(_limit * _page))
      .exec()
      .catch((_error) => {
        throw _error
      })

    const USER_COUNT = await News.find()
      .countDocuments({})
      .catch((_error) => {
        throw _error
      })

    return {
      count: USER_COUNT,
      data: NEWS_LIST
    }
  } catch (_error) {
    throw _error
  }
}

/**
 * Service to verify if a story already exist in the database
 * @param {String} _objectId Story Identificator
 * @returns Object
 */
let checkIfNewIsSavedAlreadyService = async (_objectId) => {
  try {
    return await News.findOne({ objectID: _objectId }).catch((_error) => {
      throw _error
    })
  } catch (_error) {
    throw _error
  }
}

/**
 * Service to delete a story from the database
 * @param {String} _objectId Story Identificator
 * @returns Boolean
 */
let deleteNodejsNewByIdService = async (_objectId) => {
  try {
    await News.findOneAndRemove({ objectID: _objectId }).catch((_error) => {
      throw _error
    })
    return true
  } catch (_error) {
    throw _error
  }
}

export { retrieveDataHackerNewApiService, saveManyNewsService, listNodejsNewsService, checkIfNewIsSavedAlreadyService, deleteNodejsNewByIdService }
