import mongoose from 'mongoose'

const highlightDataSubSchema = new mongoose.Schema({
  value: {
    type: String,
    required: false
  },
  matchLevel: {
    type: String,
    required: false
  },
  fullyHighlighted: {
    type: String,
    required: false
  },
  matchedWords: {
    type: [String],
    required: false
  }
})

export default highlightDataSubSchema
