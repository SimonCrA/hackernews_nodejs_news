import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

import highlightSubSchema from './highlight_result.subschema.js'

const newsSchema = new mongoose.Schema({
  story_id: {
    type: Number,
    required: true
  },
  story_title: {
    type: String,
    required: false
  },
  story_url: {
    type: String,
    required: false
  },
  parent_id: {
    type: Number,
    required: false
  },
  created_at_i: {
    type: Number,
    required: false
  },
  created_at: {
    type: Date,
    required: false
  },
  title: {
    type: String,
    required: false
  },
  url: {
    type: String,
    required: false
  },
  author: {
    type: String,
    required: false
  },
  points: {
    type: Number,
    required: false
  },
  story_text: {
    type: String,
    required: false
  },
  comment_text: {
    type: String,
    required: false
  },
  num_comments: {
    type: Number,
    required: false
  },
  _tags: {
    type: [String],
    required: false
  },
  objectID: {
    type: String,
    required: false
  },
  _highlightResult: {
    type: highlightSubSchema,
    required: true
  }
})

newsSchema.plugin(uniqueValidator, { message: '{PATH} must be unique' })

export default mongoose.model('News', newsSchema)
