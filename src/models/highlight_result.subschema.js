import mongoose from 'mongoose'

import highlightDataSubSchema from './highlight_data.subschema.js'

const highlightSubSchema = new mongoose.Schema({
  author: { type: highlightDataSubSchema },
  comment_text: { type: highlightDataSubSchema },
  story_title: { type: highlightDataSubSchema },
  story_url: { type: highlightDataSubSchema }
})

export default highlightSubSchema
