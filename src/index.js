'use-strict'
import dotenv from 'dotenv'
dotenv.config({ path: './.env' })
import cors from 'cors'
import morgan from 'morgan'
import express from 'express'
import bodyParser from 'body-parser'
const app = express()

// ----------- CORS ----------- //
app.use(cors())

// ----------- console better requests printing  ----------- //
app.use(morgan('dev'))

// ----------- Parsing requests ----------- //
// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// Parse application/json
app.use(bodyParser.json())

// ----------- import & connect routes ----------- //
import { routesConfig } from './routes/news.routes.js'
routesConfig(app)

// ----------- import & connect DB ----------- //
import { connectDb } from './services/mongoose.service.js'
const CONNECTED = await connectDb().catch((_error) => {
  console.log(_error)
})
if (CONNECTED) {
  console.log('MongoDB is connected')
}

// ----------- import & start cronjob ----------- //
import cronjob from './cronjobs/index.js'
cronjob()
console.log('CronJob will execute every hour from now on.')

// ----------- server port listening ----------- //
app.listen(process.env.SERVER_PORT, () => {
  console.log(`Server on Port ${process.env.SERVER_PORT}`)
})
