# hackernews_nodejs_news

An api to retrieve news about node.js from digital portal hackernews

---

### The server consists of 3 main APIs to fit the requirements.

1. /api/v1/load/data
   GET - retrieve the API data from the URL you provided, and saved in the server Database. (this is only after the server starts the first time)
2. /api/v1/list/nodejs/news
   GET - List news stored in the Database with pagination (page,limit) and search field.

3. /api/v1/list/nodejs/new
   DELETE - Delete a new from the database by new's objectID

Postman API online documentation is right here
(docs)[https://documenter.getpostman.com/view/8334104/Uyr5nyub]

---

## Getting started

these are the firsts steps needed to run the server

### Install modules

This command install all the modules and dependencies

```
npm install
```

---

### Running tests

This command executes all the tests availabes

```
npm run test
```

---

### Running tests with coverage report

This command executes all the test available and print a chart in the terminal showing code coverage

```
npm run nyc-test-text
```

---

### start running the server

This command starts the server on port in http://localhost:3000

```
npm run dev
```

The server requires environment variables to start, you need to create a .env file in the root path of the project and insert these variables

```
SERVER_PORT = 3000

DB_NAME = insert DB name
DB_USERNAME = insert DB username
DB_PASSWORD = insert DB password
DB_URI = insert DB uri

```

## Cronjob

The server has a cronjob set to call the hackernews external API and populate the database every hour after the server is started. (Please don't forget to stop the server)

## Docker

First you need to create a docker file in the root path

```
touch Dockerfile

```

Then you need to populated with the docker configuration.

In order to build the image you need to use this command

```

docker build -t your_dockerhub_username/hackernews_nodejs_news .

```

Then try to start it

```
docker run --name simon-hackernews-test -p 80:3000 -d your_dockerhub_username/hackernews_nodejs_news

```
